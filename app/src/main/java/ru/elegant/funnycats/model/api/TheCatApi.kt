package ru.elegant.funnycats.model.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query
import ru.elegant.funnycats.model.data.Cat

interface TheCatApi {

    @GET("images/search")
    fun searchCats(@Query("limit") count: Int = 10): Single<List<Cat>>
}