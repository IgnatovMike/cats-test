package ru.elegant.funnycats.model.data

data class Cat(
    val id: String,
    val url: String
)