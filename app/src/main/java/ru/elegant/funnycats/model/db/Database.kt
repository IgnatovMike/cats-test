package ru.elegant.funnycats.model.db

import io.reactivex.Completable
import io.reactivex.Flowable
import ru.elegant.funnycats.model.data.Cat

interface Database {

    fun insertCat(cat: Cat): Completable

    fun deleteCat(cat: Cat): Completable

    fun getCats(): Flowable<List<Cat>>
}