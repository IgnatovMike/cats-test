package ru.elegant.funnycats.model.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface RoomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCat(catEntity: CatEntity)

    @Query("SELECT * from cats where remoteId = :remoteId")
    fun getCat(remoteId: String): Single<CatEntity>

    @Delete
    fun deleteCat(catEntity: CatEntity)

    @Query("SELECT * from cats")
    fun getAll(): Flowable<List<CatEntity>>
}