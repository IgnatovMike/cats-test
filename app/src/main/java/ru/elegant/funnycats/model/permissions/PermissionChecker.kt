package ru.elegant.funnycats.model.permissions

interface PermissionChecker {

    fun checkWriteExternalStoragePermission(
        requestPermissions: (String) -> Unit,
        resultCallback: (Boolean) -> Unit
    ): (Array<out String>, result: IntArray) -> Unit

    // add more if need
}