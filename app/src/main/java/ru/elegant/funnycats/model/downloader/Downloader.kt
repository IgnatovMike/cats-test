package ru.elegant.funnycats.model.downloader

import io.reactivex.Completable
import ru.elegant.funnycats.model.data.Cat

interface Downloader {

    fun downloadCatImage(cat: Cat): Completable
}