package ru.elegant.funnycats.model.permissions

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.content.ContextCompat
import javax.inject.Inject

class PermissionCheckerImpl @Inject constructor(
    private val context: Context
) : PermissionChecker {

    override fun checkWriteExternalStoragePermission(
        requestPermissions: (String) -> Unit,
        resultCallback: (Boolean) -> Unit
    ): (Array<out String>, result: IntArray) -> Unit {

        val permission = Manifest.permission.WRITE_EXTERNAL_STORAGE

        return checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, resultCallback).also {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                return@also resultCallback(true)
            }

            if (isPermissionGranted(permission, context)) {
                return@also resultCallback(true)
            }

            requestPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
    }

    private fun checkPermission(
        permission: String,
        resultCallback: (Boolean) -> Unit
    ): (Array<out String>, result: IntArray) -> Unit = { permissions, grantResults ->
        if (permissions[0] == permission) {
            val isGranted = grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
            resultCallback(isGranted)
        }
    }

    private fun isPermissionGranted(permission: String, context: Context): Boolean =
        ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED
}