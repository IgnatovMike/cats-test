package ru.elegant.funnycats.model.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "cats")
data class CatEntity(
    val remoteId: String,
    val url: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}