package ru.elegant.funnycats.model.db

import io.reactivex.Completable
import io.reactivex.Flowable
import ru.elegant.funnycats.model.data.Cat

class DatabaseImpl(
    private val roomDatabase: CatsRoomDatabase
) : Database {

    override fun insertCat(cat: Cat): Completable = Completable.create { subscriber ->
        val catEntity = CatEntity(remoteId = cat.id, url = cat.url)
        roomDatabase.catDao().insertCat(catEntity)
        subscriber.onComplete()
    }

    override fun deleteCat(cat: Cat): Completable =
        roomDatabase.catDao().getCat(cat.id)
            .flatMapCompletable { entity -> deleteCatEntity(entity) }

    override fun getCats(): Flowable<List<Cat>> = roomDatabase.catDao().getAll().map { convertList(it) }

    private fun deleteCatEntity(catEntity: CatEntity) = Completable.create { subscriber ->
        roomDatabase.catDao().deleteCat(catEntity)
        subscriber.onComplete()
    }

    private fun convertList(dbList: List<CatEntity>): List<Cat> {
        val cats = mutableListOf<Cat>()
        dbList.forEach { cats.add(Cat(it.remoteId, it.url)) }
        return cats
    }
}