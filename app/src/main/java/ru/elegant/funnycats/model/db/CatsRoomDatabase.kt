package ru.elegant.funnycats.model.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [CatEntity::class], version = 1)
abstract class CatsRoomDatabase : RoomDatabase() {

    abstract fun catDao(): RoomDao
}