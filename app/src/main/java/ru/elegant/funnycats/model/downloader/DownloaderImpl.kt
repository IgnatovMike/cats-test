package ru.elegant.funnycats.model.downloader

import android.content.Context
import android.os.Environment
import com.bumptech.glide.Glide
import io.reactivex.Completable
import ru.elegant.funnycats.model.data.Cat
import java.io.File
import java.io.IOException
import javax.inject.Inject

class DownloaderImpl @Inject constructor(private val context: Context) : Downloader {

    @Throws(IOException::class)
    override fun downloadCatImage(cat: Cat): Completable = Completable.create { subscriber ->
        val cacheFile = Glide.with(context)
            .download(cat.url)
            .submit()
            .get()

        val lastDot = cat.url.lastIndexOf('.')
        val fileExtension = cat.url.substring(lastDot, cat.url.length)

        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            cat.id + fileExtension
        )

        cacheFile.copyTo(file)
        cacheFile.delete()

        subscriber.onComplete()
    }
}