package ru.elegant.funnycats.inject

import android.content.Context
import dagger.Component
import okhttp3.OkHttpClient
import ru.elegant.funnycats.App
import ru.elegant.funnycats.inject.modules.ApiModule
import ru.elegant.funnycats.inject.modules.AppModule
import ru.elegant.funnycats.inject.modules.DbModule
import ru.elegant.funnycats.model.api.TheCatApi
import ru.elegant.funnycats.model.db.Database
import ru.elegant.funnycats.model.downloader.Downloader
import ru.elegant.funnycats.model.permissions.PermissionChecker
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, DbModule::class])
interface AppComponent {

    fun inject(app: App)

    val catApi: TheCatApi

    val okHttpClient: OkHttpClient

    val context: Context

    val database: Database

    val downloader: Downloader

    val permissionChecker: PermissionChecker
}