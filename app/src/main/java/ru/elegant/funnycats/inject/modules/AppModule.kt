package ru.elegant.funnycats.inject.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import ru.elegant.funnycats.model.downloader.Downloader
import ru.elegant.funnycats.model.downloader.DownloaderImpl
import ru.elegant.funnycats.model.permissions.PermissionChecker
import ru.elegant.funnycats.model.permissions.PermissionCheckerImpl

@Module
class AppModule(private val context: Context) {

    private val interceptor = HttpLoggingInterceptor()
        .apply { level = HttpLoggingInterceptor.Level.BODY }

    @Provides
    fun provideOkHttp(): OkHttpClient = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    @Provides
    fun provideContext() = context

    @Provides
    fun provideDownloader(context: Context): Downloader = DownloaderImpl(context)

    @Provides
    fun providePermissionChecker(context: Context): PermissionChecker = PermissionCheckerImpl(context)
}