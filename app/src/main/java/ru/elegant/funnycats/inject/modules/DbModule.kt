package ru.elegant.funnycats.inject.modules

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import ru.elegant.funnycats.model.db.CatsRoomDatabase
import ru.elegant.funnycats.model.db.Database
import ru.elegant.funnycats.model.db.DatabaseImpl
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): Database {
        val roomDatabase = Room
            .databaseBuilder(context, CatsRoomDatabase::class.java, "database")
            .build()

        return DatabaseImpl(roomDatabase)
    }
}