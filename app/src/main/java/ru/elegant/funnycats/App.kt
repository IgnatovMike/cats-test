package ru.elegant.funnycats

import android.app.Application
import ru.elegant.funnycats.inject.AppComponent
import ru.elegant.funnycats.inject.DaggerAppComponent
import ru.elegant.funnycats.inject.modules.AppModule

class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    // https://docs.thecatapi.com/pagination

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}