package ru.elegant.funnycats.ui

import ru.elegant.funnycats.model.data.Cat

data class CatViewModel(
    val cat: Cat,
    var isFavorite: Boolean
)