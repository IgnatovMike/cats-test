package ru.elegant.funnycats.ui.search.inject

import dagger.Module
import dagger.Provides
import ru.elegant.funnycats.inject.scopes.FragmentLevel
import ru.elegant.funnycats.model.api.TheCatApi
import ru.elegant.funnycats.model.db.Database
import ru.elegant.funnycats.model.downloader.Downloader
import ru.elegant.funnycats.model.permissions.PermissionChecker
import ru.elegant.funnycats.ui.search.SearchPresenter

@Module
class SearchModule {

    @Provides
    @FragmentLevel
    fun providePresenter(
        catApi: TheCatApi,
        database: Database,
        downloader: Downloader,
        permissionChecker: PermissionChecker
    ): SearchPresenter = SearchPresenter(catApi, database, downloader, permissionChecker)
}