package ru.elegant.funnycats.ui.favorites

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.elegant.funnycats.model.data.Cat
import ru.elegant.funnycats.model.db.Database
import ru.elegant.funnycats.model.downloader.Downloader
import ru.elegant.funnycats.model.permissions.PermissionChecker
import ru.elegant.funnycats.ui.CatViewModel
import javax.inject.Inject

@InjectViewState
class FavoritesPresenter @Inject constructor(
    private val database: Database,
    private val downloader: Downloader,
    private val permissionChecker: PermissionChecker
) : MvpPresenter<FavoritesView>() {

    private val compositeDisposable = CompositeDisposable()
    private var permissionRequestResult: ((Array<out String>, result: IntArray) -> Unit)? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        viewState.showLoading()

        compositeDisposable.add(
            database.getCats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map { convertCats(it) }
                .doAfterNext { viewState.hideLoading() }
                .subscribe(
                    { list -> handleResult(list) },
                    { viewState.showToast("Something went wrong.") }
                )
        )
    }

    fun onFavoritesIconClicked(catViewModel: CatViewModel) {
        if (catViewModel.isFavorite) {
            compositeDisposable.add(
                database.deleteCat(catViewModel.cat)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnError { viewState.showToast("Something went wrong.") }
                    .subscribe()
            )
        }
    }

    fun onDownloadCatImageToFile(cat: Cat, permissionRequest: (String) -> Unit) {
        permissionRequestResult = permissionChecker.checkWriteExternalStoragePermission(
            permissionRequest
        ) { granted ->
            if (granted) {
                compositeDisposable.add(
                    downloader.downloadCatImage(cat)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { viewState.showToast("Image was downloaded") },
                            { viewState.showToast("Something went wrong.") }
                        )
                )
            } else {
                viewState.showToast("Access denied")
            }
        }
    }

    fun onRequestPermissionsResult(permissions: Array<out String>, result: IntArray) {
        permissionRequestResult?.invoke(permissions, result)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    private fun handleResult(list: List<CatViewModel>) {
        if (list.isEmpty()) {
            viewState.showEmptyState()
        } else {
            viewState.showFavorites(list)
        }
    }

    private fun convertCats(favList: List<Cat>) = favList.map { CatViewModel(it, true) }
}