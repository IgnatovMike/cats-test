package ru.elegant.funnycats.ui

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.arellomobile.mvp.MvpAppCompatFragment
import ru.elegant.funnycats.R
import ru.elegant.funnycats.ui.favorites.FavoritesFragment
import ru.elegant.funnycats.ui.search.SearchFragment

class MainActivity : AppCompatActivity() {

    private val tabLayout: TabLayout by lazy {
        findViewById<TabLayout>(R.id.tabs)
    }

    private val fragmentPager: ViewPager by lazy {
        findViewById<ViewPager>(R.id.vp_fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val searchFragment = SearchFragment()
        val favoritesFragment = FavoritesFragment()

        val searchTitle = getString(R.string.search_tab)
        val favoritesTitle = getString(R.string.favorites_tab)

        val searchPair = Pair(searchTitle, searchFragment)
        val favoritesPair = Pair(favoritesTitle, favoritesFragment)
        val tabs = listOf<Pair<String, MvpAppCompatFragment>>(searchPair, favoritesPair)

        fragmentPager.adapter = TabPagerAdapter(supportFragmentManager, tabs)

        tabLayout.setupWithViewPager(fragmentPager)
    }
}
