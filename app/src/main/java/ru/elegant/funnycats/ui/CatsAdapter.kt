package ru.elegant.funnycats.ui

import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import ru.elegant.funnycats.R
import ru.elegant.funnycats.glide.GlideApp
import ru.elegant.funnycats.model.data.Cat

class CatsAdapter : RecyclerView.Adapter<CatsAdapter.CatViewHolder>() {

    val cats = mutableListOf<CatViewModel>()

    var onItemClickListener: OnItemClickListener? = null

    fun setData(list: List<CatViewModel>) {
        cats.clear()
        cats.addAll(list)
    }

    override fun onCreateViewHolder(parant: ViewGroup, position: Int): CatViewHolder {
        val view = LayoutInflater.from(parant.context).inflate(R.layout.item_cat, parant, false)
        return CatViewHolder(view)
    }

    override fun getItemCount() = cats.size

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {

        val catViewModel = cats[position]

        val circularProgressDrawable = CircularProgressDrawable(holder.itemView.context)
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.start()

        GlideApp.with(holder.itemView.context)
            .load(catViewModel.cat.url)
            .placeholder(circularProgressDrawable)
            .into(holder.catImage)

        when (catViewModel.isFavorite) {
            true -> holder.favoritesView.setImageResource(R.drawable.ic_favorite_filled_24dp)
            false -> holder.favoritesView.setImageResource(R.drawable.ic_favorite_border_24dp)
        }

        holder.favoritesView.setOnClickListener { onItemClickListener?.onFavoritesClick(catViewModel) }
        holder.downloadView.setOnClickListener { onItemClickListener?.onDownloadClick(catViewModel.cat) }
    }

    interface OnItemClickListener {

        fun onFavoritesClick(catViewModel: CatViewModel)

        fun onDownloadClick(cat: Cat)
    }

    class CatViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val catImage: ImageView by lazy { view.findViewById<ImageView>(R.id.iv_cat_photo) }
        val favoritesView: ImageView by lazy { view.findViewById<ImageView>(R.id.iv_favorites) }
        val downloadView: ImageView by lazy { view.findViewById<ImageView>(R.id.iv_download) }
    }
}