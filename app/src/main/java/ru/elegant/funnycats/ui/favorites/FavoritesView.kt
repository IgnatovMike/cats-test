package ru.elegant.funnycats.ui.favorites

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.elegant.funnycats.ui.CatViewModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface FavoritesView : MvpView {

    fun showFavorites(list: List<CatViewModel>)

    fun showEmptyState()

    fun showLoading()

    fun hideLoading()

    fun showToast(message: String)
}