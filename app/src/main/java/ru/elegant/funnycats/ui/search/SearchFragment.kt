package ru.elegant.funnycats.ui.search

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.elegant.funnycats.App
import ru.elegant.funnycats.R
import ru.elegant.funnycats.model.data.Cat
import ru.elegant.funnycats.ui.CatListDiffUtilCallback
import ru.elegant.funnycats.ui.CatViewModel
import ru.elegant.funnycats.ui.CatsAdapter
import ru.elegant.funnycats.ui.ItemOffsetDecoration
import ru.elegant.funnycats.ui.search.inject.DaggerSearchComponent
import javax.inject.Inject

private const val PERMISSION_REQUEST_CODE = 1312

class SearchFragment : MvpAppCompatFragment(), SearchView {

    @Inject
    @InjectPresenter
    lateinit var presenter: SearchPresenter

    @ProvidePresenter
    fun providePresenter() = presenter

    private val progressBar by lazy {
        view?.findViewById<ProgressBar>(R.id.pb_loading)
    }

    private val catsRecyclerView by lazy {
        view?.findViewById<RecyclerView>(R.id.rv_cats)
    }

    private val swipeToRefresh by lazy { view?.findViewById<SwipeRefreshLayout>(R.id.swiperefresh) }

    private val catsAdapter = CatsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerSearchComponent.builder()
            .appComponent(App.appComponent)
            .build().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_search, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        swipeToRefresh?.setOnRefreshListener { presenter.onSwipeToRefresh() }

        catsRecyclerView?.addItemDecoration(ItemOffsetDecoration(context!!, R.dimen.dp_8))
        catsRecyclerView?.adapter = catsAdapter
        catsRecyclerView?.layoutManager = GridLayoutManager(context, 2)

        catsAdapter.onItemClickListener = object : CatsAdapter.OnItemClickListener {

            override fun onFavoritesClick(catViewModel: CatViewModel) = presenter.onCatAddedToFavorites(catViewModel)

            override fun onDownloadClick(cat: Cat) {
                presenter.onDownloadCatImageToFile(cat) { permission ->
                    requestPermissions(arrayOf(permission), PERMISSION_REQUEST_CODE)
                }
            }
        }
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showCats(list: List<CatViewModel>) {
        swipeToRefresh?.isRefreshing = false

        val diffUtilCallback = CatListDiffUtilCallback(catsAdapter.cats, list)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback, true)
        catsAdapter.setData(list)
        diffResult.dispatchUpdatesTo(catsAdapter)
    }

    override fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            presenter.onRequestPermissionsResult(permissions, grantResults)
        }
    }
}