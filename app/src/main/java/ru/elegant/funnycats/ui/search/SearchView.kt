package ru.elegant.funnycats.ui.search

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.elegant.funnycats.ui.CatViewModel

@StateStrategyType(AddToEndSingleStrategy::class)
interface SearchView : MvpView {

    fun showLoading()

    fun hideLoading()

    fun showCats(list: List<CatViewModel>)

    fun showToast(message: String)
}