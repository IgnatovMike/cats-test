package ru.elegant.funnycats.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.arellomobile.mvp.MvpAppCompatFragment

class TabPagerAdapter(
    fragmentManager: FragmentManager,
    private val list: List<Pair<String, MvpAppCompatFragment>>
) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = list[position].second

    override fun getCount(): Int = list.size

    override fun getPageTitle(position: Int): CharSequence? = list[position].first
}