package ru.elegant.funnycats.ui.search

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.elegant.funnycats.model.api.TheCatApi
import ru.elegant.funnycats.model.data.Cat
import ru.elegant.funnycats.model.db.Database
import ru.elegant.funnycats.model.downloader.Downloader
import ru.elegant.funnycats.model.permissions.PermissionChecker
import ru.elegant.funnycats.ui.CatViewModel
import javax.inject.Inject

@InjectViewState
class SearchPresenter @Inject constructor(
    private val catApi: TheCatApi,
    private val database: Database,
    private val downloader: Downloader,
    private val permissionChecker: PermissionChecker
) : MvpPresenter<SearchView>() {

    private val compositeDisposable = CompositeDisposable()
    private var permissionRequestResult: ((Array<out String>, result: IntArray) -> Unit)? = null

    private val favoritesCatList = mutableListOf<Cat>()
    private val apiCatList = mutableListOf<Cat>()

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        compositeDisposable.add(
            database.getCats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { handleFavoritesResult(it) },
                    { viewState.showToast("Something went wrong.") }
                )
        )
        requestCats()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    fun onSwipeToRefresh() = requestCats()

    fun onCatAddedToFavorites(catViewModel: CatViewModel) {

        // what to do with current state
        val disposable = when (catViewModel.isFavorite) {
            true -> database.deleteCat(catViewModel.cat)
            false -> database.insertCat(catViewModel.cat)
        }
        compositeDisposable.add(
            disposable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { }
        )
    }

    fun onDownloadCatImageToFile(cat: Cat, permissionRequest: (String) -> Unit) {
        permissionRequestResult = permissionChecker.checkWriteExternalStoragePermission(
            permissionRequest
        ) { granted ->
            if (granted) {
                compositeDisposable.add(
                    downloader.downloadCatImage(cat)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                            { viewState.showToast("Image was downloaded") },
                            { viewState.showToast("Something went wrong.") }
                        )
                )
            } else {
                viewState.showToast("Access denied")
            }
        }
    }

    fun onRequestPermissionsResult(permissions: Array<out String>, result: IntArray) {
        permissionRequestResult?.invoke(permissions, result)
    }

    private fun requestCats() {

        viewState.showLoading()

        compositeDisposable.add(
            catApi.searchCats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate { viewState.hideLoading() }
                .subscribe(
                    { cats -> handleApiResult(cats) },
                    { viewState.showToast("Something went wrong.") }
                )
        )
    }

    private fun handleCatLists(favoritesList: List<Cat>, apiList: List<Cat>) {
        val result = apiList.map { CatViewModel(it, isFavorite = favoritesList.contains(it)) }
        viewState.showCats(result)
    }

    private fun handleFavoritesResult(favoritesList: List<Cat>) {
        favoritesCatList.clear()
        favoritesCatList.addAll(favoritesList)
        handleCatLists(favoritesCatList, apiCatList)
    }

    private fun handleApiResult(apiList: List<Cat>) {
        apiCatList.clear()
        apiCatList.addAll(apiList)
        handleCatLists(favoritesCatList, apiCatList)
    }
}