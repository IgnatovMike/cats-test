package ru.elegant.funnycats.ui.favorites

import android.os.Bundle
import android.support.constraint.Group
import android.support.v7.util.DiffUtil
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.elegant.funnycats.App
import ru.elegant.funnycats.R
import ru.elegant.funnycats.model.data.Cat
import ru.elegant.funnycats.ui.CatListDiffUtilCallback
import ru.elegant.funnycats.ui.CatViewModel
import ru.elegant.funnycats.ui.CatsAdapter
import ru.elegant.funnycats.ui.ItemOffsetDecoration

private const val PERMISSION_REQUEST_CODE = 4112

class FavoritesFragment : MvpAppCompatFragment(), FavoritesView {

    private val favoritesRecyclerView by lazy { view?.findViewById<RecyclerView>(R.id.rv_favorites) }
    private val progressBar by lazy { view?.findViewById<ProgressBar>(R.id.pb_loading) }
    private val emptyStateGroup by lazy { view?.findViewById<Group>(R.id.group_empty_state) }

    private val catsAdapter = CatsAdapter()

    @InjectPresenter
    lateinit var presenter: FavoritesPresenter

    @ProvidePresenter
    fun providePresenter() = FavoritesPresenter(
        App.appComponent.database,
        App.appComponent.downloader,
        App.appComponent.permissionChecker
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_favorites, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoritesRecyclerView?.addItemDecoration(ItemOffsetDecoration(context!!, R.dimen.dp_8))
        favoritesRecyclerView?.adapter = catsAdapter
        favoritesRecyclerView?.layoutManager = GridLayoutManager(context, 2)

        catsAdapter.onItemClickListener = object : CatsAdapter.OnItemClickListener {

            override fun onFavoritesClick(catViewModel: CatViewModel) = presenter.onFavoritesIconClicked(catViewModel)

            override fun onDownloadClick(cat: Cat) = presenter.onDownloadCatImageToFile(cat) { permission ->
                requestPermissions(arrayOf(permission), PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            presenter.onRequestPermissionsResult(permissions, grantResults)
        }
    }

    override fun showFavorites(list: List<CatViewModel>) {
        favoritesRecyclerView?.visibility = View.VISIBLE
        emptyStateGroup?.visibility = View.GONE

        val diffUtilCallback = CatListDiffUtilCallback(catsAdapter.cats, list)
        val diffResult = DiffUtil.calculateDiff(diffUtilCallback, true)
        catsAdapter.setData(list)
        diffResult.dispatchUpdatesTo(catsAdapter)
    }

    override fun showEmptyState() {
        favoritesRecyclerView?.visibility = View.GONE
        emptyStateGroup?.visibility = View.VISIBLE
    }

    override fun showLoading() {
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressBar?.visibility = View.GONE
    }

    override fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show()
    }
}