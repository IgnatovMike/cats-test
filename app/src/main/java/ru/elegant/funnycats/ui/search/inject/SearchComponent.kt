package ru.elegant.funnycats.ui.search.inject

import dagger.Component
import ru.elegant.funnycats.inject.AppComponent
import ru.elegant.funnycats.inject.scopes.FragmentLevel
import ru.elegant.funnycats.ui.search.SearchFragment
import ru.elegant.funnycats.ui.search.SearchPresenter

@Component(dependencies = [AppComponent::class])
@FragmentLevel
interface SearchComponent {

    fun inject(fragment: SearchFragment)

    val presenter: SearchPresenter
}