package ru.elegant.funnycats.ui

import android.support.v7.util.DiffUtil

class CatListDiffUtilCallback(
    private val oldList: List<CatViewModel>,
    private val newList: List<CatViewModel>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition].cat.id == newList[newItemPosition].cat.id

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
        oldList[oldItemPosition] == newList[newItemPosition]
}